<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
            'full_name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'address' => 'kathmandu',
            'phone' => 9873638848,
            'gender' => 'M',
            'role' => 'SA',
            'password' => Hash::make('admin002'),
        ]);
    }
}
