<?php


namespace App\Observers;
use App\Models\Patient;
use Carbon\Carbon;
use App\Http\Controllers\DateConverterController;

class PatientObserver
{
    public function created(Patient $patient)
    {
        $convert_date = new  DateConverterController();
        $date_time = Carbon::now('+5:45');
        $bs_date = ($convert_date->eng_to_nep($date_time->year, $date_time->month, $date_time->day));
        $patient->his_no = $bs_date['year'].'-'.$patient->getKey();
        $patient->save();
    }
}
