<?php

namespace App\Providers;

use App\Models\Patient;
use App\Observers\PatientObserver;
use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Patient::observe(PatientObserver::class);
    }
}
