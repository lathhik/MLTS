<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = ['patient_id', 'test_list_id', 'test_list_value', 'remarks'];

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function testLists()
    {
        return $this->belongsTo(TestList::class, 'test_list_id');
    }


}
