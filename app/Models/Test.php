<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'tests';
    protected $fillable = ['name', 'code'];

    public function testLists()
    {
        return $this->hasMany(TestList::class, 'test_id');
    }
}
