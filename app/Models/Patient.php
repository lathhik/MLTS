<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patients';
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'age', 'email', 'gender', 'address', 'phone', 'consultant', 'patient_category', 'his_no'];

    public function samples()
    {
        return $this->hasMany(Sample::class);
    }

    public function testLists()
    {
        return $this->belongsToMany(TestList::class, 'samples');
    }

    public function sampleType()
    {
        return $this->hasMany(SampleType::class);
    }

    public function getFullNameAttribute()
    {
        return ucwords($this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name);
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'patient_id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

}
