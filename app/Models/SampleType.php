<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SampleType extends Model
{
    protected $table = 'sample_types';
    protected $fillable = ['patient_id', 'type'];

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
}
