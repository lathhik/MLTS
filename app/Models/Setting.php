<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = ['name', 'phone_1', 'phone_1', 'address_district', 'address_area', 'image_logo'];
}
