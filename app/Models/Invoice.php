<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $fillable = ['patient_id', 'total_amount', 'due_amount', 'status'];

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}
