<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class TestList extends Model
{
    protected $table = 'test_lists';
    protected $fillable = ['test_id', 'parent_id', 'name', 'unit', 'normal', 'abnormal', 'ref_range', 'description', 'cost'];

    public function test()
    {
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function subCategories()
    {
        return $this->hasMany(TestList::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(TestList::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function samples()
    {
        return $this->belongsTo(Sample::class);
    }

}


