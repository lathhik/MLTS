<?php

namespace App\Models;

Use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'admins';
    protected $fillable = ['full_name', 'username', 'address', 'email', 'gender', 'phone', 'password', 'image', 'remember_token', 'role', 'status'];
}
