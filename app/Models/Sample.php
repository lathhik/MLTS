<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
    protected $table = 'samples';
    protected $fillable = ['patient_id', 'test_list_id', 'status'];

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function testLists()
    {
        return $this->belongsTo(TestList::class,'test_list_id');
    }
}
