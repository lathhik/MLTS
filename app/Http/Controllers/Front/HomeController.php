<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use App\Models\Report;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function homepage()
    {
        $reports =collect();
        if(\request()->has('his_no') && strlen(\request()->his_no)>2)
            $reports = Report::whereHas('patient',function ($q){
                $q->where('his_no','like','%'.\request()->input('his_no').'%');
            })->get();
        return view('welcome',compact('reports'));
    }
}
