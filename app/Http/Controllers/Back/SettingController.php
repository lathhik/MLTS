<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::first();
        return view('settings.setting', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone_1' => 'required',
            'address_district' => 'required',
            'address_area' => 'required',
            'image_logo' => 'required',
        ]);

        $status = $request->input('status');
        settype($status, 'integer');
        if ($request->hasFile('image_logo')) {
            $file = $request->file('image_logo');
            $file_name = Str::random(20) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('assets/images/setting'), $file_name);
        }

        $settings = Setting::where('status', 1)->first();
        if ($settings == null) {
            $settings = new Setting();
            $settings->name = $request->input('name');
            $settings->phone_1 = $request->input('phone_1');
            $settings->phone_2 = $request->input('phone_2');
            $settings->address_district = $request->input('address_district');
            $settings->address_area = $request->input('address_area');
            $settings->image_logo = $file_name;
            $settings->status = $status;
            $settings->save();
            return redirect()->back();
        } else {
            $settings->name = $request->input('name');
            $settings->phone_1 = $request->input('phone_1');
            $settings->phone_2 = $request->input('phone_2');
            $settings->address_district = $request->input('address_district');
            $settings->address_area = $request->input('address_area');
            $settings->image_logo = $file_name;
            $settings->status = $status;
            $settings->save();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
