<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStoreRequest;
use App\Http\Requests\AdminUpdateRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Null_;

class AdminController extends Controller
{

    /**
     * Display available admins except authenticated
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::where('id', '!=', Auth::guard('admin')->user()->id)->get();
        return view('pages.admin.view_admins', compact('admins'));
    }


    /**
     * Show the form for creating a new admin/Staffs
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create_admin');
    }


    /**
     * Store a newly created admins/staffs in database
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStoreRequest $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file_name = Str::random(30) . '.' . $file->getClientOriginalExtension();
            $file->move('assets/images/admin/', $file_name);
        }

        $admin = Admin::create([
            'full_name' => $request->input('full_name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'gender' => $request->input('gender'),
            'role' => $request->input('role'),
            'password' => Hash::make($request->input('password')),
            'image' => $file_name ?? '',
        ]);

        if ($admin) {
            return redirect()->route('admin.index')->with('success', 'Successfully created');
        }
        return redirect()->back()->with('fail', 'Something went wrong, Please try again');

    }


    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified admin/staff
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        return view('pages.admin.edit_admin', compact('admin'));
    }

    /**
     * Update the specified admin/staff in storage
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, $id)
    {
        $admin = Admin::findOrFail($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file_name = Str::random(30) . '.' . $file->getClientOriginalExtension();
            if (public_path('assets/images/admin/' . $admin->image)) {
                unlink(public_path() . '/assets/images/admin/' . $admin->image);
            }
            $file->move('assets/images/admin/', $file_name);
        }

        $admin->full_name = $request->input('full_name');
        $admin->username = $request->input('username');
        $admin->email = $request->input('email');
        $admin->address = $request->input('address');
        $admin->phone = $request->input('phone');
        $admin->gender = $request->input('gender');
        $admin->role = $request->input('role');
        $admin->image = $file_name ?? '';

        if ($admin->save()) {
            return redirect()->route('admin.index')->with('success', 'Update successfully');
        }
        return redirect()->route('admin.index')->with('fail', 'Something went wrong');

    }


    /**
     * Update Status of Specific Admin/Staffs
     * @param $id
     * @return
     */
    public function updateStatus($id)
    {
        $admin = Admin::findOrFail($id);
        if ($admin->status == 1) {
            $admin->status = 0;
        } elseif ($admin->status == 0) {
            $admin->status = 1;
        }
        if ($admin->save()) {
            return redirect()->back()->with('success', 'Status updated successfully');
        }
        return redirect()->back()->with('fail', 'Something went wrong');
    }

    /**
     * Remove the specified admin/staff and related files from storage
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        if (public_path('assets/images/admin/' . $admin->image) && $admin->image != null) {
            unlink(public_path() . '/assets/images/admin/' . $admin->image);
        }

        if ($admin->delete()) {
            return redirect()->back()->with('success', 'Deleted successfully');
        }
        return redirect()->back()->with('fail', 'Something went wrong');
    }
}
