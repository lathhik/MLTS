<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Invoice;
use App\Models\Patient;
use App\Models\Sample;
use App\Models\Setting;
use App\Models\TestList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\DateConverterController;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{

    public function viewToGenerateInvoice()
    {
        $invoices = Invoice::query();
        if (\request()->has('search') && \request()->search != null) {
            $search = \request()->search;
            $invoices->where('inv_no', 'like', '%' . $search . '%')->orWhereHas('patient', function ($q) use ($search) {
                $q->where('first_name', 'like', '%' . $search . '%')->orwhere('phone', 'like', '%' . $search . '%')->orwhere('his_no', 'like', '%' . $search . '%');
            })->latest();
        }
        $invoices = $invoices->where('status', 'unpaid')->latest()->paginate(20);
        return view('pages.invoice.view_to_generate_invoice', compact('invoices'));
    }

    public function generate($id)
    {
        $settings = Setting::first();
        $convert_date = new DateConverterController();
        $patient = Patient::findOrFail($id);
        $samples = Sample::where('patient_id', $patient->id)->where('status', 'pending')->get();
        $invoice = Invoice::where('patient_id', $patient->id)->where('status', 'unpaid')->first();
        $date_time = Carbon::now('+5:45');
        $bs_date = ($convert_date->eng_to_nep($date_time->year, $date_time->month, $date_time->day));
        $total_costs = 0;
        $admin = Auth::guard('admin')->user();

        foreach ($patient->samples as $s) {
            if ($s->status == 'pending') {
                $total_costs += $s->testLists->cost;
            }
        }
        $discount = $total_costs * (10 / 100);
        return view('pages.invoice.view_invoice', compact('patient', 'total_costs', 'discount', 'date_time', 'bs_date', 'settings', 'admin', 'invoice','samples'));
    }

    public function paid()
    {
        $invoice = Invoice::where('patient_id', \request()->id)->where('status', 'unpaid')->first();
        $invoice->status = 'paid';
        $invoice->save();
        return redirect()->route('back.view_to_generate_invoice');
    }

}
