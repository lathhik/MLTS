<?php

namespace App\Http\Controllers\Back\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function show()
    {

    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email_username' => 'required',
            'password' => 'required'
        ]);

        $login_type = filter_var($request->email_username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $request->merge([
            $login_type => $request->email_username
        ]);

        if (Auth::guard('admin')->attempt($request->only($login_type, 'password'))) {
            return redirect()->intended(route('index'));
        }

        return redirect()->back()->with('fail', 'Invalid Credentials');

    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('login');
    }
}
