<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Patient;
use App\Models\Report;
use App\Models\Sample;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if (\request()->search) {
            $search_key = \request()->search;
            $patients = Patient::query();
            $patients->where('first_name', 'like', '%' . $search_key . '%')
                ->orWhere('phone', 'like', '%' . $search_key . '%')
                ->orWhere('his_no', 'like', '%' . $search_key . '%');
            $patients = $patients->latest()->paginate(20);
            return view('pages.patient.view_patients', compact('patients'));
        }
        $patients = Patient::all()->count();
        $total_tests = Sample::all()->count();
        $patient_daily = Patient::whereDate('created_at', Carbon::today())->count();
        $total_tests_daily = Sample::whereDate('created_at', Carbon::today())->count();
        $tests_done_daily = Sample::where('status', 'done')->whereDate('updated_at', Carbon::today())->count();
        $tests_pending_daily = Sample::where('status', 'pending')->whereDate('created_at', Carbon::today())->count();
        $admin = Auth::guard('admin')->user();
        return view('pages.home', compact('patients',
            'patient_daily', 'total_tests_daily',
            'tests_done_daily', 'tests_pending_daily',
            'total_tests', 'admin'));
    }
}
