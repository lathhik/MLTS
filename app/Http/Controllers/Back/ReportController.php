<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Patient;
use App\Models\Report;
use App\Models\Sample;
use App\Models\SampleType;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::query();
        $search = \request()->search;
        if (\request()->has('search') && \request()->search != null)
            $reports->whereHas('patient', function ($q) use ($search) {
                $q->where('first_name', 'like', '%' . $search . '%')->orWhere('phone', 'like', '%' . $search . '%');
            });
        $reports = $reports->where('status', null)->latest()->paginate(200);
        return view('pages.report.view_to_generate_report', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = \request()->id;
        $patient = Patient::findOrFail($id);
        $sample_types = \Opis\Closure\unserialize(SampleType::where('patient_id', $patient->id)->latest()->value('type'));
        $samples = Sample::where('patient_id', $patient->id)->where('status', '!=', 'done')->latest()->get();
//        dd($samples);
        return view('pages.report.input_test_results', compact('patient', 'sample_types', 'samples'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->input('reports') as $key => $value) {
            $report = new Report();
            $report->patient_id = $request->input('patient_id');
            $report->test_list_id = $key;
            $report->test_list_value = $value;
            $report->save();
        }

        $patient = Patient::findOrFail($request->input('patient_id'));
        $patient->status = 'done';
        $patient->save();

        $samples = Sample::where('patient_id', $request->input('patient_id'))->where('status', '!=', 'done')->latest()->get();
        foreach ($samples as $sample) {
            $sample->status = 'done';
            $sample->save();
        }
        $sample_type = SampleType::where('patient_id', $patient->id)->where('status', 'collected')->first();
        $sample_type->status = 'done';
        $sample_type->save();
        return redirect()->route('report.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::findOrFail($id);
        $reports = Report::where('patient_id', $id)->where('status', null)->get();
        $admin = Auth::guard('admin')->user();
        $date_time = Carbon::now('+5:45');
        $settings = Setting::first();
        return view('pages.report.view_report', compact('patient', 'reports', 'admin', 'date_time', 'settings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::findOrFail($id);
        $patient = Patient::where('id', $report->patient_id)->first();
        $sample_types = \Opis\Closure\unserialize(SampleType::where('patient_id', $patient->id)->latest()->value('type'));
        $samples = Sample::where('patient_id', $report->patient_id)->latest()->get();
        return view('pages.report.edit_test_results', compact('patient', 'report', 'sample_types', 'samples'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::where('id', $request->input('patient_id'))->first();
        $reports = Report::where('patient_id', $request->input('patient_id'))->where('status', null)->get();
        $test_list_ids = [];
        $test_list_values = [];
        foreach ($request->input('reports') as $key => $value) {
            array_push($test_list_ids, $key);
            array_push($test_list_values, $value);
        }
        $i = 0;
        foreach ($reports as $report) {
            for ($j = $i; $j <= count($reports);) {
                $report->patient_id = $patient->id;
                $report->test_list_id = $test_list_ids[$i];
                $report->test_list_value = $test_list_values[$i];
                $report->save();
                break;
            }
            $i++;
        }
        return redirect()->back()->with('success', 'Test results updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function generate()
    {
        $reports = Report::where('patient_id', \request()->id)->where('status', null)->get();
        foreach ($reports as $r) {
            $r->status = 'generated';
            $r->save();
        }
        return redirect()->route('report.index');
    }

    public function destroy($id)
    {
        //
    }
}
