<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DateConverterController;
use App\Http\Requests\PatientStoreRequest;
use App\Models\Invoice;
use App\Models\Patient;
use App\Models\Sample;
use App\Models\SampleType;
use App\Models\TestList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use function PHPSTORM_META\type;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::query();
        if (\request()->has('search')) {
            $search_key = \request()->input('search');
            $patients->where('first_name', 'like', '%' . $search_key . '%')
                ->orWhere('phone', 'like', '%' . $search_key . '%')
                ->orWhere('his_no', 'like', '%' . $search_key . '%');
        }
        $patients = $patients->latest()->paginate(20);
        return view('pages.patient.view_patients', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patient = null;
        $test_lists = TestList::where('parent_id', null)->get();
//        foreach ($test_lists as $tl){
//            dd($tl);
//        }
//        dd($test_lists);
        if (\request()->id) {
            $patient = Patient::where('id', \request()->id)->first();
//            dd($patient->testLists);
            return view('pages.patient.create_patient', compact('test_lists', 'patient'));
        }
        return view('pages.patient.create_patient', compact('test_lists', 'patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientStoreRequest $request)
    {

        if ($request->input('old_patient') == null) {
            //================== create new patient =================//
            $patient = new Patient();
            $patient->first_name = $request->input('first_name');
            $patient->middle_name = $request->input('middle_name');
            $patient->last_name = $request->input('last_name');
            $patient->address = $request->input('address');
            $patient->phone = $request->input('phone');
            $patient->email = $request->input('email');
            $patient->gender = $request->input('gender');
            $patient->age = $request->input('age');
            $patient->consultant = $request->input('consultant');
            $patient->patient_category = $request->input('patient_category');
            $patient->save();
        } else {
            //====================== Old Patient ============================//
            $patient = Patient::findOrFail($request->input('old_patient'));
            $patient->first_name = $request->input('first_name');
            $patient->middle_name = $request->input('middle_name');
            $patient->last_name = $request->input('last_name');
            $patient->address = $request->input('address');
            $patient->phone = $request->input('phone');
            $patient->email = $request->input('email');
            $patient->gender = $request->input('gender');
            $patient->age = $request->input('age');
            $patient->consultant = $request->input('consultant');
            $patient->patient_category = $request->input('patient_category');
            $patient->status = null;
            $patient->save();
        }

        //================== create sample of specific patient =====================//
        $tests = $request->input('tests');
        $test_list_id = [];
        $sample_code = mt_rand(10000000, 99999999);
        foreach ($tests as $test) {
            array_push($test_list_id, $test);
            $sample = new Sample();
            $sample->patient_id = $patient->id;
            $sample->test_list_id = $test;
            $sample->sample_code = $sample_code;
            $sample->save();
        }

        //================ create invoice of specific patient ============================//
        $invoice = new Invoice();
        $invoice->patient_id = $patient->id;

        $convert_date = new  DateConverterController();
        $date_time = Carbon::now('+5:45');
        $bs_date = ($convert_date->eng_to_nep($date_time->year, $date_time->month, $date_time->day));
        $year_now = $bs_date['year'] % 100;
        $year_next = ($bs_date['year'] + 1) % 100;
        $rand_no = mt_rand(10000, 99999);
        $inv_no = $year_now . '-' . $year_next . '/' . $bs_date['month'] . '/' . $rand_no;
        $invoice->inv_no = $inv_no;
        $total_amount = 0;
        foreach ($test_list_id as $tld) {
            $single_cost = TestList::where('id', $tld)->value('cost');
            $total_amount += $single_cost;
        }
        $invoice->total_amount = $total_amount;
        $invoice->save();
        return redirect()->route('back.view_to_generate_invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test_lists = TestList::where('parent_id', null)->get();
        $patient = Patient::findOrFail($id);
        return view('pages.patient.edit_patient', compact('patient', 'test_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::findOrFail($id);
        $invoice_status = Invoice::where('patient_id', $patient->id)->value('status');
        $sample = Sample::where('patient_id', $patient->id)->first();

        if ($invoice_status == 'unpaid' && $sample->status == 'pending') {

            $patient->first_name = $request->input('first_name');
            $patient->middle_name = $request->input('middle_name');
            $patient->last_name = $request->input('last_name');
            $patient->address = $request->input('address');
            $patient->phone = $request->input('phone');
            $patient->email = $request->input('email');
            $patient->gender = $request->input('gender');
            $patient->age = $request->input('age');
            $patient->consultant = $request->input('consultant');
            $patient->patient_category = $request->input('patient_category');
            $patient->save();


            //================== update sample of specific patient =====================//
            $tests = $request->input('tests');
            $test_list_id = [];
            $sample_code = mt_rand(10000000, 99999999);
            foreach ($patient->samples as $sample) {
                $sample->delete();
            }
            foreach ($tests as $test) {
                array_push($test_list_id, $test);
                $sample = new Sample();
                $sample->patient_id = $patient->id;
                $sample->test_list_id = $test;
                $sample->sample_code = $sample_code;
                $sample->save();
            }

            //================ create invoice of specific patient ============================//
            $invoice = Invoice::where('patient_id', $patient->id)->first();
            $total_amount = 0;
            foreach ($test_list_id as $tld) {
                $single_cost = TestList::where('id', $tld)->value('cost');
                $total_amount += $single_cost;
            }
            $invoice->total_amount = $total_amount;
            $invoice->save();

            return redirect()->route('patient.index')->with('success', 'Patient updated successfully');

        } else {
            $patient->first_name = $request->input('first_name');
            $patient->middle_name = $request->input('middle_name');
            $patient->last_name = $request->input('last_name');
            $patient->address = $request->input('address');
            $patient->phone = $request->input('phone');
            $patient->email = $request->input('email');
            $patient->gender = $request->input('gender');
            $patient->age = $request->input('age');
            $patient->consultant = $request->input('consultant');
            $patient->patient_category = $request->input('patient_category');
            $patient->save();
            return redirect()->route('patient.index')->with('success', 'Can only update patient details');
        }
    }


    /**
     * Show old patient's details with their respective tests and reports
     */
    public function patientDetails()
    {
        $patient_id = \request()->id;
        $patient = Patient::where('id', \request()->id)->first();
        return view('pages.patient.view_patient_details', compact('patient'));
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrFail($id);
        try {
            $patient->delete();
            return redirect()->back()->with('success', 'Patient deleted successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('fail', 'Cannot delete patient');
        }
    }
}
