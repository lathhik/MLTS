<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PasswordResetController extends Controller
{
    public function showReset()
    {
        return view('pages.profile.show_reset');
    }

    public function resetAction(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $admin_password = $admin->getAuthPassword();

        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        if (Hash::check($request->old_password, $admin_password)) {
            $admin->fill([
                'password' => Hash::make($request->password)])->save();
            return redirect()->route('profile.index')->with('success', 'Password was successfully reset');
        } else {
            return redirect()->back()->with('fail', 'Old password do not match');
        }
    }
}
