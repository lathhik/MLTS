<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Test;
use App\Models\TestList;
use Illuminate\Http\Request;

class TestListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test_lists = TestList::all();
        return view('pages.test_list.view_test_lists', compact('test_lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test_categories = Test::all();
        $test_lists = TestList::where('parent_id', null)->get();
        return view('pages.test_list.create_test_list', compact('test_categories', 'test_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'test_list_name' => 'required',
            'test_category' => 'required',
        ]);

        $test_list = new TestList();
        $test_list->test_id = $request->input('test_category');
        $test_list->name = $request->input('test_list_name');
        $test_list->unit = $request->input('unit');
        $test_list->cost = $request->input('cost');
        $test_list->ref_range = serialize($request->input('ref_range'));
        $test_list->normal = $request->input('normal');
        $test_list->abnormal = $request->input('abnormal');
        $test_list->description = $request->input('description');
        if ($request->has('test_list_id') && $request->test_list_id != null) {
            $test_list->parent_id = $request->input('test_list_id');
        }
        if ($test_list->save()) {
            return redirect()->route('test-list.index')->with('success', 'Test list successfully created');
        }
        return redirect()->back()->with('fail', 'Something went wrong, Please create again');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test_categories = Test::all();
        $test_lists = TestList::all();
        $test_list = TestList::findOrFail($id);
        return view('pages.test_list.edit_test_list', compact('test_list', 'test_categories', 'test_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test_list = TestList::findOrFail($id);
        $this->validate($request, [
            'test_list_name' => 'required',
            'test_category' => 'required',

        ]);

        $test_list->test_id = $request->input('test_category');
        $test_list->name = $request->input('test_list_name');
        $test_list->unit = $request->input('unit');
        $test_list->cost = $request->input('cost');
        $test_list->ref_range = serialize($request->input('ref_range'));
        $test_list->normal = $request->input('normal');
        $test_list->abnormal = $request->input('abnormal');
        $test_list->description = $request->input('description');


        if ($request->has('test_list_id') && $request->test_list_id != null) {
            $test_list->parent_id = $request->input('test_list_id');
        } else {
            $test_list->parent_id = null;
        }

        if ($test_list->save()) {
            return redirect()->route('test-list.index')->with('success', 'Test List successfully updated');
        }
        return redirect()->route('test-list.index')->with('success', 'Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test_list = TestList::findOrFail($id);
        try {
            $test_list->delete();
            return redirect()->back()->with('success', 'Test list deleted');
        } catch (\Exception $e) {
            return redirect()->back()->with('fail', 'Cannot delete test list');
        }
    }
}
