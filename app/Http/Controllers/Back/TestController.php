<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all();
        return view('pages.test.view_test', compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.test.create_test');
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'test_category_name' => 'required',
            'code' => 'required|unique:tests,code',
        ]);

        $test_category = Test::create([
            'name' => $request->input('test_category_name'),
            'code' => $request->input('code'),
        ]);

        if ($test_category) {
            return redirect()->route('test.index')->with('success', 'Test Category created');
        }
        return redirect()->route('test.index')->with('fail', 'Something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::findOrFail($id);
        return view('pages.test.edit_test', compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = Test::findOrFail($id);
        $this->validate($request, [
            'test_category_name' => 'required|unique:tests,code,' . $id
        ]);
        $test->name = $request->input('test_category_name');
        $test->code = $request->input('code');
        if ($test->save()) {
            return redirect()->route('test.index')->with('success', 'Test Category updated');
        }
        return redirect()->route('test.index')->with('fail', 'Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Test $test)
    {
        try{
            $test->delete();
            return redirect()->back()->with('success', 'Test Category Deleted');
        }
        catch(\Exception $e){
            return redirect()->back()->with('fail', 'Something went wrong');
        }
    }
}
