<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use App\Models\Sample;
use App\Models\SampleType;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    public function viewSample()
    {
        $samples = Sample::query();
        $search = \request()->search;
        if (\request()->has('search') && \request()->search != null) {
            $samples->whereHas('patient', function ($q) use ($search) {
                $q->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search . '%');
            })->orWhere('sample_code', 'like', '%' . $search, '%');
        }
        $samples = $samples->where('status', '!=', 'done')->latest()->paginate(200);
        return view('pages.sample.view_sample', compact('samples'));
    }

    public function collect($id)
    {
        $patient = Patient::find($id);
        $samples = Sample::where('patient_id', $patient->id)->where('status', '!=', 'done')->latest()->get();
        return view('pages.sample.collect_sample', compact('patient', 'samples'));
    }

    public function collectAction(Request $request)
    {
        $this->validate($request, [
            'samples' => 'required'
        ]);
        $sample_types = new SampleType();
        $sample_types->patient_id = $request->patient_id;
        $sample_types->type = serialize($request->samples);
        $sample_types->status = 'collected';
        $sample_types->save();

        $samples = Sample::where('patient_id', $request->patient_id)->where('status', '!=', 'done')->latest()->get();
        foreach ($samples as $sample) {
            $sample->status = 'processing';
            $sample->save();
        }
        return redirect()->route('back.view_sample');
    }


    public function editSample($id)
    {
        $patient = Patient::findOrFail($id);
        $samples = Sample::where('patient_id', $patient->id)->where('status', '!=', 'done')->latest()->get();
        $sample_type = SampleType::where('patient_id', $patient->id)->latest()->first();
        $sample_types = $sample_type->type;
        return view('pages.sample.edit_sample', compact('patient', 'sample_types', 'samples'));

    }

    public function updateSample(Request $request)
    {
        $patient = Patient::findOrfail($request->patient_id);
        $sample_type = SampleType::where('patient_id', $patient->id)->latest()->first();
        $sample_type->type = serialize($request->samples);
        $sample_type->save();
        return redirect()->route('back.view_details', $patient->id)->with('success', 'Patient sample type updated');
    }

    public function viewDetails($id)
    {
        $patient = Patient::findOrFail($id);
        $samples = Sample::where('patient_id', $patient->id)->where('status', '!=', 'done')->latest()->get();
        $sample_types = SampleType::where('patient_id', $patient->id)->where('status', '!=', 'done')->latest()->first();
        if (!$sample_types) {
            $sample_types = SampleType::where('patient_id', $patient->id)->where('status', 'collected')->latest()->first();
        }
        return view('pages.sample.view_details', compact('patient', 'samples', 'sample_types'));
    }
}
