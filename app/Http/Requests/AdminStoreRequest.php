<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required', 'regex:/^[a-zA-Z\s]*/',
            'username' => 'required|max:20|min:5|unique:admins,username',
            'email' => 'required|email|unique:admins,email',
            'address' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'role' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function messages()
    {
        return [
            'full_name.regex' => 'Full only contains letters'
        ];
    }
}
