<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{

    public function handle($request, Closure $next, ...$guards)
    {
        $user = Auth::guard('admin')->user();
        if ($user) {
            foreach ($guards as $guard) {
                if ($guard == $user->role) {
                    $this->authenticate($request, $guards);
                    return $next($request);
                }
            }
            return abort(403);
        }
        return redirect()->route('login');
    }


    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }

}
