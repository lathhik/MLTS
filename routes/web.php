<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'Front\HomeController@homepage');

Route::group(['namespace' => 'Back'], function () {
    Route::get('/login', 'HomeController@login')->name('login')->middleware('guest:admin');
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login-action', 'LoginController@login')->name('login_action')->middleware('guest:admin');
        Route::get('logout', 'LoginController@logout')->name('logout');
    });

    Route::group(['middleware' => 'auth:admin,SA,RT,LT'], function () {
        Route::get('/dashboard', 'DashboardController@index')->name('index');
        Route::resource('profile', 'ProfileController');
        Route::get('reset-password', 'PasswordResetController@showReset')->name('back.show_reset');
        Route::post('reset-password-action', 'PasswordResetController@resetAction')->name('back.reset_action');
    });

    Route::group(['middleware' => 'auth:admin,SA'], function () {
        Route::resource('admin', 'AdminController');
        Route::get('update-status/{id}', 'AdminController@updateStatus')->name('admin.update_status');
        Route::resource('settings', 'SettingController');
    });

    Route::group(['middleware' => 'auth:admin,SA,RT',], function () {
        Route::resource('patient', 'PatientController', ['excepts' => 'create']);
        Route::get('patient/create/{id?}', ['as' => 'patient.create', 'uses' => 'PatientController@create']);
        Route::get('patient/details/{id}', ['as' => 'patient.details', 'uses' => 'PatientController@patientDetails']);
        Route::get('view-generate-invoice', 'InvoiceController@viewToGenerateInvoice')->name('back.view_to_generate_invoice');
        Route::get('generate-invoice/{id}', 'InvoiceController@generate')->name('back.generate_invoice');
        Route::get('invoice-paid/{id}','InvoiceController@paid')->name('back.invoice_paid');

    });

    Route::group(['middleware' => 'auth:admin,SA,LT',], function () {
        Route::resource('test', 'TestController');
        Route::resource('test-list', 'TestListController');
        Route::get('view-sample', 'SampleController@viewSample')->name('back.view_sample');
        Route::get('collect-sample/{id}', 'SampleController@collect')->name('back.collect_sample');
        Route::post('collect-sample', 'SampleController@collectAction')->name('back.collect_action');
        Route::get('edit-sample/{id}', 'SampleController@editSample')->name('back.edit_sample');
        Route::post('update-sample', 'SampleController@updateSample')->name('back.update_sample');
        Route::get('view-details/{id}', 'SampleController@viewDetails')->name('back.view_details');
        Route::resource('report', 'ReportController', ['excepts' => 'create']);
        Route::get('report/create/{id}', ['as' => 'report.create', 'uses' => 'ReportController@create']);
        Route::get('report/generate/{id}','ReportController@generate')->name('back.report_generate');
    });
});



