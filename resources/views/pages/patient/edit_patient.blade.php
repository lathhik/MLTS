@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center mb-0">
               <h4  class="text-primary mb-0"> Edit Patient</h4>
            </div>
            <div class="card-body">
                <div class="">
                    <form action="{{route('patient.update',$patient->id)}}" method="post" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="row col-md-12">
                            <div class="form-group col-md-4">
                                <label for="">First Name</label>
                                <input type="text" class="form-control" name="first_name"
                                       value="{{$patient->first_name}}">
                                @error('first_name')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Middle Name</label>
                                <input type="text" class="form-control" name="middle_name"
                                       value="{{$patient->middle_name}}">
                                @error('middle_name')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control" name="last_name"
                                       value="{{$patient->last_name}}">
                                @error('last_name')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-4">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" value="{{$patient->email}}">
                                @error('email')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Address</label>
                                <input type="text" class="form-control" name="address" value="{{$patient->address}}">
                                @error('address')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{$patient->phone}}">
                                @error('phone')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-4">
                                <label for="">Gender</label><br>
                                <input type="radio" name="gender" value="M"
                                       id="male" {{$patient->gender == 'M'?'checked':''}}>&nbsp;<label
                                    for="male">Male</label>&nbsp;
                                <input type="radio" name="gender" value="F"
                                       id="female" {{$patient->gender == 'F'?'checked':''}}>&nbsp;<label
                                    for="female">Female</label>&nbsp;
                                <input type="radio" name="gender" value="O"
                                       id="others" {{$patient->gender == 'O'?'checked':''}}>&nbsp;<label
                                    for="others">Others</label>
                                @error('gender')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Age</label>
                                <input type="number" class="form-control" name="age" placeholder="eg. 30" max="120"
                                       min="1"
                                       value="{{$patient->age}}">
                                @error('age')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Consultant</label>
                                <input type="text" class="form-control" name="consultant"
                                       placeholder="eg. Dr. John Snow"
                                       value="{{$patient->consultant}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Patient Category</label>
                                <select name="patient_category" id="" class="form-control">
                                    <option value="" selected disabled>Select Patient Category</option>
                                    <option value="general" {{$patient->patient_category == 'general'?'selected':''}}>
                                        General
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Test List</label>
                                <select name="tests[]" id="" class="form-control select-tests" multiple="multiple">
                                    @foreach($test_lists as $tl)
                                        <option
                                            value="{{$tl->id}}"{{$patient->testLists->contains($tl->id)?'selected':''}}>{{$tl->name}}</option>
                                    @endforeach
                                </select>
                                @error('tests')
                                <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="text-center mt-2 mb-4">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
            $('.select-tests').select2({
                placeholder: 'Select Tests',
                theme: 'classic',
            });
        });
    </script>
@stop
