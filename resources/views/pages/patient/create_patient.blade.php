@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center">
                <h4 class="mb-0 text-primary text-bold">Create Patient</h4>
            </div>
            <div class="card-body">
                <form action="{{route('patient.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{$patient?$patient->id:null}}" name="old_patient">
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">First Name</label>
                            <input type="text" class="form-control" name="first_name"
                                   value="{{$patient?$patient->first_name:old('first_name')}}" placeholder="eg. Purna">
                            @error('first_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Middle Name</label>
                            <input type="text" class="form-control" name="middle_name"
                                   value="{{$patient?$patient->middle_name:old('middle_name')}}"
                                   placeholder="eg. Bahadur">
                            @error('middle_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Last Name</label>
                            <input type="text" class="form-control" name="last_name"
                                   value="{{$patient?$patient->last_name:old('last_name')}}" placeholder="eg. Magar">
                            @error('last_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email"
                                   value="{{$patient?$patient->email:old('email')}}"
                                   placeholder="eg. example@gmail.com">
                            @error('email')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="address"
                                   value="{{$patient?$patient->address:old('address')}}"
                                   placeholder="eg. Baneshowr -12 , Kathmandu">
                            @error('address')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="phone"
                                   value="{{$patient?$patient->phone:old('phone')}}" placeholder="eg. 9876383748">
                            @error('phone')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Gender</label><br>
                            <input type="radio" name="gender" value="M"
                                   id="male" @if($patient){{$patient->gender == 'M'?'checked':''}}@else {{old('gender') == 'M'?'checked':''}} @endif>&nbsp;<label
                                for="male">Male</label>&nbsp;
                            <input type="radio" name="gender" value="F"
                                   id="female" @if($patient){{$patient->gender == 'F'?'checked':''}}@else {{old('gender') == 'F'?'checked':''}} @endif>&nbsp;<label
                                for="female">Female</label>&nbsp;
                            <input type="radio" name="gender" value="O"
                                   id="others" @if($patient){{$patient->gender == 'O'?'checked':''}}@else {{old('gender') == 'O'?'checked':''}} @endif>&nbsp;<label
                                for="others">Others</label>
                            @error('gender')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Age</label>
                            <input type="number" class="form-control" name="age" placeholder="eg. 30" max="120" min="1"
                                   value="{{$patient?$patient->age:old('age')}}">
                            @error('age')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Consultant</label>
                            <input type="text" class="form-control" name="consultant" placeholder="eg. Dr. John Snow"
                                   value="{{$patient?$patient->consultant:old('consultant')}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Patient Category</label>
                            <select name="patient_category" id="" class="form-control">
                                <option value="" selected disabled>Select Patient Category</option>
                                <option
                                    value="general" @if($patient && $patient->patient_category == 'general'){{'selected'}} @elseif(old('general')== 'general') {{'selected'}}@endif>
                                    General
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Test List</label>
                            <select name="tests[]" id="" class="form-control select-tests" multiple="multiple">
                                @foreach($test_lists as $tl)
                                    <option
                                        value="{{$tl->id}}"
                                    @if($patient)
                                        @foreach($patient->testLists as $ptl)
                                            {{$ptl->test_id == $tl->test_id?'selected':''}}
                                            @endforeach
                                        @else
                                        {{collect(old('tests'))->contains($tl->id)?'selected':''}}
                                        @endif>{{$tl->name}}
                                    </option>
                                @endforeach
                            </select>
                            @error('tests')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="text-center mt-2 mb-4">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
            $('.select-tests').select2({
                placeholder: 'Select Tests',
                theme: 'classic',
            });
        });
    </script>
@stop
