@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card">
            <div class="card-header col-md-12 card-head">
                <h3 class="m-0 font-weight-bold text-primary d-inline-block col-md-6 mt-1">Patient Lists</h3>
                <form action="" class="float-right col-md-6">
                    <input type="text" class="form-control d-inline col-md-10 search-input" placeholder="search patient"
                           value="{{request()->search}}" name="search">
                    <button class="btn btn-primary ml-1"><span class="fa fa-search"></span></button>
                </form>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>His_no</th>
                            <th>Full Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Age</th>
                            <th width="11%">Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($patients as $patient)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$patient->his_no}}</td>
                                <td>{{$patient->getFullNameAttribute()}}</td>
                                <td>{{$patient->address}}</td>
                                <td>{{$patient->phone}}</td>
                                <td>{{$patient->gender}}</td>
                                <td>{{$patient->age}}</td>
                                <td>
                                    @if(!$patient->status)
                                        <a href="{{route('patient.edit',$patient->id)}}"
                                           class="btn btn-sm btn-secondary mb-1"><span
                                                class="fa fa-edit"></span></a>
                                        <form action="{{route('patient.destroy', $patient->id)}}" method="post"
                                              class="form-check-inline">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-danger"
                                                    onclick="return(confirm('Are you Sure??'))"><span
                                                    class="fa fa-trash"></span>
                                            </button>
                                        </form>
                                    @endif
                                    @if($patient->status)
                                        <a href="{{route('patient.create',['id'=>$patient->id])}}"
                                           class="btn btn-sm btn-primary mb-1 "><span
                                                class="fa fa-plus-circle"></span></a>
                                        <a href="{{route('patient.details',['id'=>$patient->id])}}"
                                           class="btn btn-sm btn-primary mb-1 mr-2"><span
                                                class="fa fa-eye"></span></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{$patients->links()}}
                </div>
            </div>

        </div>

    </div>
    <!-- /.container-fluid -->
@stop
