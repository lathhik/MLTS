@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        @include('messages.message')
        <div class="">
            <div class="card mb-4">
                <div class="card-header text-center text-primary">
                    <h4 class="mb-0"> Patient Details</h4>
                </div>
                <div class="card-body">
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->getFullNameAttribute()}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Age</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->age}} yrs" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Gender</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->gender}}" readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->address}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->phone}}" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Patient Category</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->patient_category}}"
                                   readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Consultant</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->consultant}}" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <div class="text-center">
                                <label for="" style="font-size: 20px" class=""><b>Tests History</b></label>
                            </div>
                            @php
                                $samples = $patient->samples->sortByDesc('sample_code');
                            @endphp
                            <div class="">
                                <table class="table table-bordered view-table-details table-hover" id="dataTable" width="100%"
                                       cellspacing="0">
                                    <thead class="text-center">
                                    <tr>
                                        <th>Tests Name</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">
                                    @foreach($samples as $s)
                                        @if($loop->iteration != 1)
                                            @if($previous_sample != $s->sample_code)
                                                <td style="background: #7ea2b4"></td>
                                                <td style="background: #7ea2b4"></td>
                                            @endif
                                        @endif
                                        @php($previous_sample = $s->sample_code)
                                        <tr>
                                            <td>
                                                @if($s->testLists->subCategories)
                                                    {{$s->testLists->name}}
                                                    @foreach($s->testLists->subCategories as $sc)
                                                        *{{$sc->name}} {{$loop->last?'':','}}
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$s->created_at)->format('d M ,Y') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center mt-4">
                                <a href="{{route('patient.create',['id'=>$patient->id])}}" class="btn btn-primary">Add</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
            $('.sample-type').select2({
                placeholder: 'Select Sample Type/s'
            });
        });
    </script>
@stop
