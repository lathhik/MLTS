@extends('master')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card mb-4">
            <div class="card-header col-md-12 card-head">
                <h3 class="m-0 font-weight-bold text-primary d-inline-block col-md-6 mt-1">Invoice Lists</h3>
                <form action="" class="col-md-6 d-inline-block  float-right ">
                    <input type="text" class="form-control d-inline col-md-10 search-input" name="search"
                           value="{{request()->h_no}}" placeholder="search invoices">
                    <button class="btn btn-primary d-inline"><span class="fa fa-search"></span></button>
                </form>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th width="13%">INV.No</th>
                            <th>Full Name</th>
                            <th width="11%">Total Cost</th>
                            <th>Status</th>
                            <th width="12%">Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$invoice->inv_no}}</td>
                                <td>{{$invoice->patient->getFullNameAttribute()}}</td>
                                <td>Rs. {{$invoice->total_amount}}</td>
                                <td>{{ucfirst($invoice->status)}}</td>
                                <td>
                                    @if($invoice->status == 'unpaid')
                                        <a href="{{route('back.generate_invoice',$invoice->patient->id)}}"
                                           class="btn btn-sm btn-secondary">Generate&nbsp;<i
                                                class="fa fa-arrow-alt-circle-right"></i>
                                        </a>

                                    @else
                                        <a href="" class="btn btn-sm btn-secondary">Generated
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
