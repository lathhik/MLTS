@extends('master')
@section('content')
    <div class="print text-center">
        <a href="{{route('back.view_to_generate_invoice')}}" class="btn btn-secondary ml-4 d-print-none float-left">Back</a>
        <button class="btn btn-secondary ml-2 d-print-none" onclick="window.print()">Print</button>
        <a href="{{route('back.invoice_paid',$patient->id)}}" class="btn btn-success mr-3 float-right">Paid</a>

    </div>
    <div class="container-fluid invoice" id="invoice">
        <section class="header">
            <div class="row">
                <div class="col-md-2">
                    <img class="logo-section float-left"
                         src="{{$settings ? asset('assets/images/setting/'.$settings->image_logo):''}}">
                </div>
                <div class="col-md-8 text-center text-uppercase">
                    <h4 class="font-weight-bold">{{$settings->name??''}}</h4><br>
                    <span>
                        {{$settings->address_area??''}} , {{$settings->address_district??''}}
                    </span>
                    <br>
                    <p>
                        <small>Ph. {{$settings->phone_1??''}} , {{$settings->phone_2??''}}</small>
                        <br>
                    </p>
                    <h6 class="font-weight-bold">Invoice Bill</h6>
                </div>
            </div>
            <div class="bar-codes">
                <span class="float-left">
                    {!! \Milon\Barcode\Facades\DNS1DFacade::getBarcodeHTML($patient->his_no,'C128',2.3,33) !!}
                </span>
            </div>
            <div class="bar-codes" style="margin-bottom: 40px">
                <span class="float-right">
                    {!! \Milon\Barcode\Facades\DNS1DFacade::getBarcodeHTML($patient->his_no,'C128',2.3,33) !!}
                </span>
            </div>
        </section>
        <hr>
        <!-- Patient Info Details -->
        <section class="patient-details font-weight-bold">
            <div class="row">
                <div class="col-8 text-left">
                    <div class="row">
                        <div class="col-4">
                            <p>
                                HIS NO.<br>
                                Patient Name<br>
                                Pat. Category <br>
                                Mobile No <br>
                                Consultant <br>
                                Address <br>
                            </p>
                        </div>
                        <div class="col-8">
                            <p class="text-uppercase">
                                : {{$patient->his_no}} <br>
                                : {{$patient->getFullNameAttribute()}} ({{$patient->age}} Y / {{$patient->gender}})
                                <br>
                                : {{$patient->patient_category}} <br>
                                : {{$patient->phone}}<br>
                                : {{$patient->consultant}} <br>
                                : {{$patient->address}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="row">
                        <div class="col-4">
                            <p>
                                INV No. <br>
                                Inv. Date <br>
                                BS Date <br>
                            </p>
                        </div>
                        <div class="col-8">
                            <p class="text-uppercase">
                                : {{$invoice->inv_no}}<br>
                                : {{$date_time->isoFormat('D/M/Y')}} <br>
                                : {{$bs_date['date']}}/{{$bs_date['month']}}/{{$bs_date['year']}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Test Rate and Amount -->
        <section class="test-section">
            <div class="table-responsive">
                <table class="table test-table">
                    <thead>
                    <tr>
                        <th style="width:8%;">SN</th>
                        <th style="width: 50%;">Description</th>
                        <th style="width: 13.33%">Qty</th>
                        <th style="width: 15.33%;">Rate</th>
                        <th style="width: 13.33%;">Amount</th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @foreach($samples as $s)
                        @if($s->status == 'pending')
                            <tr class="test-tr">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$s->testLists->name}}</td>
                                <td>1</td>
                                <td>{{$s->testLists->cost}}</td>
                                <td>{{$s->testLists->cost * 1}}</td>
                            </tr>
                        @endif
                    @endforeach
                    <!-- Total Bill Amount -->
                    </tbody>
                    <tbody class="font-weight-bold">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Bill Amount</td>
                        <td>Rs. {{$total_costs}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Discount</td>
                        <td>Rs. {{$discount}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total Amount</td>
                        <td>Rs. {{$total_costs - $discount}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <!-- Amount Summary -->
        <section class="summary">
            <h5 class="font-weight-bold">Rupees in Words: {{numberIntoWords($total_costs - $discount)}} Only/-</h5>
            <br>
            <p>
                Remarks: staff family charity<br><br>
                Bill User: {{$admin->full_name}} <br>
                Print Date & Time : {{$date_time->isoFormat('D/M/Y h:mm:ss a')}}
            </p>
        </section>
    </div>
@stop
@section('script')
    <script>
        function printDiv(invoice) {
            var printContents = document.getElementById("invoice").innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@stop
