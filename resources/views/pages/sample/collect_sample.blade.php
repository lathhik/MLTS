@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-primary text-center">
                <h4 class="mb-0">Collect Patient Sample Type/s</h4>
            </div>
            <div class="card-body">
                <form action="{{route('back.collect_action')}}" method="post">
                    @csrf
                    <input type="hidden" value="{{$patient->id}}" name="patient_id">
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->getFullNameAttribute()}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Age</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->age}} yrs" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Gender</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->gender}}" readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for="">Required Tests</label>
                            <ul>
                                @foreach($samples as $s)
                                    <li>{{$s->testLists->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="from-group col-md-6">
                            <label for="">Select Sample Type</label>
                            <select name="samples[]" id="" class="form-control sample-type" multiple="multiple">
                                <option value="blood">Blood</option>
                                <option value="urine">Urine</option>
                                <option value="stool">Stool</option>
                                <option value="swab">Swab</option>
                            </select>
                            @error('samples')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 text-center mb-4">
                        <button type="submit" class="btn btn-primary">Collect</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
           $('.sample-type').select2({
               placeholder:'Select Sample Type/s'
           });
        });
    </script>
@stop
