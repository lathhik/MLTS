@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card mb-4">
            <div class="card-header col-md-12  card-head">
                <h3 class="m-0 font-weight-bold text-primary d-inline-block col-md-6 mt-1">Patient's Samples </h3>
                <form action="" class="col-md-6 d-inline-block float-right">
                    <input type="text" class="form-control d-inline col-md-10 search-input" name="search" value="{{request()->search}}">
                    <button class="btn btn-primary d-inline"><span class="fa fa-search"></span></button>
                </form>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Sample Code</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($samples->unique('patient_id') as $sample)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$sample->patient->getFullNameAttribute()}}</td>
                                <td>{{$sample->patient->age}} Yrs</td>
                                <td>{{$sample->patient->gender}}</td>
                                <td>{{$sample->sample_code}}</td>
                                <td class="{{$sample->status == 'pending'?'text-danger':'text-success'}}">{{ucfirst($sample->status)}}</td>
                                <td>
                                    @if($sample->status == 'pending')
                                        <a href="{{route('back.collect_sample',$sample->patient->id)}}"
                                           class="btn btn-sm btn-danger mb-1"><span
                                                class="">Collect</span>
                                        </a>
                                    @elseif($sample->status == 'processing')
                                        <a href="{{route('report.create',['id'=>$sample->patient->id])}}"
                                           class="btn btn-sm btn-success">
                                            <span class="">Done&nbsp;&nbsp;</span>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('back.view_details',$sample->patient->id)}}"
                                       class="btn btn-sm btn-secondary ml-2">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                </td>
                            </tr>
                            @continue
                        @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{$samples->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
