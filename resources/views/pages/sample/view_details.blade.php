@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        @include('messages.message')
        <div class="">
            <div class="card mb-4">
                <div class="card-header text-center text-primary">
                    <h4 class="mb-0"> Patient Details</h4>
                </div>
                <div class="card-body">
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->getFullNameAttribute()}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Age</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->age}} yrs" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Gender</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->gender}}" readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->address}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->phone}}" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Patient Category</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->patient_category}}"
                                   readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Consultant</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->consultant}}" readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for=""><b>Required Tests</b></label>
                            <ul>
                                @foreach($samples as $s)
                                    <li>{{$s->testLists->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="from-group col-md-6">
                            <label for=""><b>Sample Type/s</b></label>
                            @if($sample_types && $sample_types->status == 'collected')
                                @foreach(unserialize($sample_types->type) as $st)
                                    <ul>
                                        <li>{{ucfirst($st)}}</li>
                                    </ul>
                                @endforeach
                            @else
                                <p class="text-danger ml-4">Sample Type/s of this patient has not been collected
                                    yet.</p>
                            @endif
                        </div>
                    </div>
                    <div class="mt-4 text-center">
                        <a href="{{route('back.view_sample')}}" class="btn btn-primary mr-2">Back</a>
                        @if($sample_types && $sample_types->status)
                            <a href="{{route('back.edit_sample',$patient->id)}}" class="btn btn-primary">Edit</a>
                        @else
                            <a href="{{route('back.collect_sample',$patient->id)}}" class="btn btn-primary">Collect</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
            $('.sample-type').select2({
                placeholder: 'Select Sample Type/s'
            });
        });
    </script>
@stop
