@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0">Edit Test Category</h4>
            </div>
            <div class="mt-4 col-md-12">
                <form action="{{route('test.update', $test->id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Test Category Name</label>
                            <input type="text" class="form-control" name="test_category_name" value="{{$test->name}}">
                            @error('test_category_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="from-group col-md-6">
                            <label for="">Test Code</label>
                            <input type="text" class="form-control" name="code" value="{{$test->code}}">
                            @error('code')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 text-center mb-4">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
