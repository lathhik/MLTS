@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-primary text-center">
                <h4 class="mb-0">Create Test Category</h4>
            </div>

            <div class="card-body">
                <form action="{{route('test.store')}}" method="post" class="">
                    @csrf
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Test Category Name</label>
                            <input type="text" class="form-control" name="test_category_name"
                                   value="{{old('test_category_name')}}" placeholder="eg. Haematology">
                            @error('test_category_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="from-group col-md-6">
                            <label for="">Test Code</label>
                            <input type="text" class="form-control" name="code" value="{{old('code')}}" placeholder="eg. T407">
                            @error('code')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
