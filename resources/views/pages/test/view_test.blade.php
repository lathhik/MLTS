@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card mb-4">
            <div class="card-header">
                <h3 class="m-0 font-weight-bold text-primary">Test Categories
                    <a href="{{route('test.create')}}" class="btn btn-sm btn-primary float-right"><li class="fa fa-plus">&nbsp;Create Test Category</li></a>
                </h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($tests as $test)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$test->name}}</td>
                                <td>{{$test->code}}</td>
                                <td>
                                    <a href="{{route('test.edit',$test->id)}}" class="btn btn-sm btn-secondary mb-1"><span
                                            class="fa fa-edit"></span></a>
                                    <form action="{{route('test.destroy', $test->id)}}" method="post" class="form-check-inline">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger"><span
                                                class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
