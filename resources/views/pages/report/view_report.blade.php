@extends('master')
@section('content')

    <div class="print text-center d-print-none">
        <a href="{{route('report.index')}}" class="btn btn-secondary ml-4 float-left">Back</a>
        <button class="btn btn-secondary ml-2 d-print-none" onclick="window.print()">Print</button>
        <a href="{{route('back.report_generate',$patient->id)}}" class="btn btn-success mr-4 float-right">Finished</a>
    </div>
    <div class="container-fluid report">
        <section class="header">
            <div class="row">
                <div class="col-md-2">
                    <img class="logo-section float-left"
                         src="{{$settings?asset('assets/images/setting/'.$settings->image_logo):''}}">
                </div>
                <div class="col-md-8 text-center text-uppercase">
                    <h4 class="font-weight-bold mt-4">{{$settings->name??''}}</h4>
                    <span>
                        {{$settings->address_area??''}} , {{$settings->address_district??''}}
                    </span>
                    <br>
                    <p>
                        <small>Ph. {{$settings->phone_1??''}} , {{$settings->phone_2??''}}</small>
                        <br>
                    </p>

                    <h6 class="font-weight-bold"><u>Lab Investigation Report</u></h6>
                </div>
            </div>
            <div class="bar-codes" style="margin-bottom: 40px">
                <span class="float-left">
                    {!! \Milon\Barcode\Facades\DNS1DFacade::getBarcodeHTML($patient->his_no,'C128',2.3,33) !!}
                </span>
                <span class="float-right">
                    {!! \Milon\Barcode\Facades\DNS1DFacade::getBarcodeHTML($patient->his_no,'C128',2.3,33) !!}
                </span>
            </div>
        </section>
        <hr>
        <!-- Patient Info Details -->
        <section class="patient-details">
            <div class="row">
                <div class="col-7 text-left">
                    <div class="row">
                        <div class="col-4">
                            <p class="text-uppercase font-weight-bold">
                                HIS NO.<br>
                                Patient Name<br>
                                Category <br>
                                Age/Gender <br>
                                Referred by <br>
                            </p>
                        </div>
                        <div class="col-8">
                            <p class="text-uppercase">
                                : {{$patient->his_no}} <br>
                                : {{$patient->getFullNameAttribute()}}<br>
                                : {{$patient->patient_category}}<br>
                                : {{$patient->age}} Y / {{$patient->gender}}<br>
                                : {{$patient->consultant}} <br>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="row">
                        <div class="col-6">
                            <p class="text-uppercase font-weight-bold">
                                Sample No. <br>
                                Sampe collected <br>
                                Report Authorised <br>
                                BS Date <br>
                            </p>
                        </div>
                        <div class="col-6">
                            <p class="text-uppercase">
                                : {{$patient->samples[0]['sample_code']}}<br>
                                : {{$patient->samples[0]['created_at']}} <br>
                                : {{$patient->reports[0]['created_at']}} <br>
                                : 19/03/2076 <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Test Name and Results Table -->
        <section class="test-results-title">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="text-center">
                    <th class="test-name">Test Name</th>
                    <th class="test-result">Result</th>
                    <th class="test-units">units</th>
                    <th style="width: 4% ;">Flag</th>
                    <th class="test-reference">Reference Range</th>
                    </thead>
                </table>
                @php($previous_test_id = 0)
                @php($previous_parent_id = 0)
                @foreach($reports as $r)
                    <table class="table test-results-table">
                        @if($r->testLists->test_id != $previous_test_id)
                            @if(!$loop->first)
                                <br>
                            @endif
                            <h6 class="text-center font-weight-bold text-uppercase">{{$r->testLists->test->name}}</h6>
                        @endif
                        @php($previous_test_id = $r->testLists->test_id)
                        <tbody class="text-center">
                        @if($r->testLists->parent && $r->testLists->parent->id != $previous_parent_id)
                            <tr>
                                <td class="test-name text-uppercase">{{$r->testLists->parent->name}}
                                    ({{$r->testLists->parent->description}})
                                </td>
                                <td class="test-result text-center">{{\App\Models\Report::where('test_list_id',$r->testLists->parent->id)->value('test_list_value')}}</td>
                                <td class="test-units text-center">{{$r->testLists->parent->unit}}</td>
                                <td class="test-flag text-center"></td>
                                <td>{{unserialize($r->testLists->parent->ref_range)}}</td>
                            </tr>
                            @php($previous_parent_id = $r->testLists->parent->id)
                        @endif
                        <tr>
                            <td class="test-name text-capitalize">{{$r->testLists->name}}</td>
                            <td class="test-result">{{$r->test_list_value}}</td>
                            <td class="test-units">{{$r->testLists->unit}}</td>
                            <td class="test-flag"></td>
                            <td>{{unserialize($r->testLists->ref_range)}}</td>
                        </tr>
                        </tbody>
                    </table>
                @endforeach

            </div>
        </section>
        <hr style="border-color: #000;">
        <br>
        <!-- Signatures and printed date -->
        <section class="signature-section">
            <div class="signature">
                <hr style="width: 80%;">
                <h6 class="font-weight-bold text-uppercase">{{$admin->full_name}}</h6>
                <span>Med. Lab Technologist (Lab In charge)</span>
                <br>
            </div>
            <br>
            <p class="ml-4">

                Print Date & Time : {{$date_time->isoFormat('D-M-Y h:mm:ss a')}}
            </p>
        </section>
        <div style="margin-top: 300px;" class="text-center">
            <p>------ End of the Report -----</p>
        </div>
    </div>
@stop
@section('script')
@stop
