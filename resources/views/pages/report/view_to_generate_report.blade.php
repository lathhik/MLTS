@extends('master')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card mb-4">
            <div class="card-header col-md-12 card-head">
                <h3 class="m-0 font-weight-bold text-primary d-inline-block col-md-6 mt-1">Report Lists</h3>
                <form action="" class="d-inline-block col-md-6 float-right">
                    <input type="text" class="form-control col-md-10 d-inline-block search-input" name="search"
                           value="{{request()->search}}">
                    <button class="btn btn-primary d-inline-block"><span class="fa fa-search"></span></button>
                </form>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>Full Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Sample Code</th>
                            <th>Status</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($reports->unique('patient_id') as $report)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$report->patient->getFullNameAttribute()}}</td>
                                <td>{{$report->patient->age}}</td>
                                <td>{{$report->patient->gender}}</td>
                                <td>{{$report->patient->samples[0]['sample_code']}}</td>
                                <td>{{ucfirst($report->status)}}</td>
                                <td>
                                    @if($report->status == null)
                                        <a href="{{route('report.edit',$report->id)}}" class="btn btn-sm btn-secondary"><i
                                                class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{route('report.show',$report->patient->id)}}"
                                           class="btn btn-sm btn-secondary">Generate&nbsp;<i
                                                class="fa fa-arrow-alt-circle-right"></i>
                                        </a>
                                    @else
                                        <span class="text-uppercase text-danger">Finished</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="float-right">
                        {{$reports->links()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
