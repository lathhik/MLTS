@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        @include('messages.message')
        <div class="card">
            <div class="card-header text-primary text-center">
                <h4 class="mb-0">Edit Patient's Test Result/s</h4>
            </div>
            <div class="card-body">
                <form action="{{route('report.update',$report->id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <input type="hidden" value="{{$patient->id}}" name="patient_id">
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control" name="patient_name"
                                   value="{{$patient->getFullNameAttribute()}}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Age</label>
                            <input type="text" class="form-control" name="age" value="{{$patient->age}} yrs" readonly>
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Gender</label>
                            <input type="text" class="form-control" name="gender" value="{{$patient->gender}}" readonly>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for=""><b>Sample Types</b></label>
                            <ul>
                                @foreach($sample_types as $st)
                                    <li>{{$st}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for=""><b>Input Test Results Value</b></label>
                            <table class="">
                                <thead>
                                <tr>
                                    <th>Tests</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($patient->reports as $pr)
                                    @if($pr->status != 'generated')
                                        <tr>
                                            <td style="padding: 15px 15px 15px 10px"
                                                class="mb-4">{{$pr->testLists->name}}</td>
                                            @if($pr->testLists->subCategories != null)
                                                @foreach($pr->testLists->subCategories as $sc)
                                                    <td style="padding-right: 5px">
                                                        {{$sc->name}}
                                                        <input type="text" class="form-control"
                                                               name="reports[{{$sc->id}}]"
                                                               value="{{$sc->test_list_value}}" required>
                                                    </td>
                                                @endforeach
                                            @endif
                                            @if(count($pr->testLists->subCategories) == null)
                                                <td style="padding: 0">
                                                    <input type="text" class="form-control"
                                                           name="reports[{{$pr->testLists->id}}]"
                                                           value="{{$pr->test_list_value}}" required>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mt-2 mb-4 text-center">
                        <a href="{{route('report.index')}}" class="btn btn-primary mr-2">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
