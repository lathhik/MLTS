@extends('master')
@section('content')

    <!-- Begin Page Content -->
    <div class="container">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-0 text-gray-800">Input Patient's Test Result/s</h1>
        </div>
        <hr style="margin-top: 5px!important;" class="mb-4">
        <div class="">
            <form action="{{route('report.store')}}" method="post">
                @csrf
                <input type="hidden" value="{{$patient->id}}" name="patient_id">
                <div class="row col-md-12">
                    <div class="form-group col-md-4">
                        <label for="">Full Name</label>
                        <input type="text" class="form-control" name="patient_name"
                               value="{{$patient->getFullNameAttribute()}}" readonly>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">Age</label>
                        <input type="text" class="form-control" name="age" value="{{$patient->age}} yrs" readonly>
                    </div>
                    <div class="from-group col-md-4">
                        <label for="">Gender</label>
                        <input type="text" class="form-control" name="gender" value="{{$patient->gender}}" readonly>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="form-group col-md-6">
                        <label for=""><b>Sample Types</b></label>
                        <ul>
                            @foreach($sample_types as $st)
                                <li>{{$st}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="form-group col-md-12">
                        <label for=""><b>Input Test Results Value</b></label>
                        <table class="">
                            <thead>
                            <tr>
                                <th>Tests</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($samples as $s)
                                <tr>
                                    <td style="padding: 15px 15px 15px 10px" class="mb-4">{{$s->testLists->name}}</td>
                                @if($s->testLists->subCategories)
                                    @foreach($s->testLists->subCategories as $sc)
                                            <td style="padding-right: 5px">
                                                {{$sc->name}}
                                                <input type="text" class="form-control" name="reports[{{$sc->id}}]"
                                                       required>
                                            </td>
                                        @endforeach
                                    @endif
                                @if(count($s->testLists->subCategories) == null)
                                        <td style="padding: 0">
                                            <input type="text" class="form-control" name="reports[{{$s->testLists->id}}]" required>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mt-4 text-center mb-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
