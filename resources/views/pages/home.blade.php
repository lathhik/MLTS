@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if($admin->role == 'SA')
            <div class="card">
                <div class="card-header text-center text-primary">
                    <h5 class="mb-0">Daily Report</h5>
                </div>
                <div class="row card-body">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total
                                            Patient
                                            (Daily)
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$patient_daily}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user-injured fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total
                                            Tests
                                            (Daily)
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_tests_daily}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-vials fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tests done
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div
                                                    class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$tests_done_daily}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-vial fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pending Requests Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tests
                                            Pending
                                        </div>
                                        <div
                                            class="h5 mb-0 font-weight-bold text-gray-800">{{$tests_pending_daily}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-question fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header text-primary text-center">
                    <h5 class="mb-0">Totals</h5>
                </div>
                <div class="row card-body">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total
                                            Patient
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$patients}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user-injured fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total
                                            Tests
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_tests}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-vials fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @elseif($admin->role == 'RT')
            <div class="card">
                <div class="card-header text-center text-primary">
{{--                    <h5 class="m-0 font-weight-bold text-primary d-inline-block col-md-6 mt-1">Dashboard</h5>--}}
                    <form action="" class="float-right col-md-6">
                        <input type="text" class="form-control d-inline col-md-10 search-input" placeholder="search patient"
                               value="{{request()->search}}" name="search">
                        <button class="btn btn-primary ml-1"><span class="fa fa-search"></span></button>
                    </form>
                </div>
                <div class="row card-body">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('patient.create')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">
                                                Create New Patient
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user-injured fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('patient.index')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">View
                                                Patients
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-user-injured fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('back.view_to_generate_invoice')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">View
                                                Invoice
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-money-bill fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @elseif($admin->role == 'LT')
            <div class="card">
                <div class="card-header text-center text-primary">
                    <h5 class="mb-0">Dashboard/Tests</h5>
                </div>
                <div class="row card-body">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('test.create')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">
                                                Create Test Categories
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-fw fa-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('test.index')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">View
                                                Test Categories
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-fw fa-list-alt fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('test-list.create')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">Create
                                                Tests
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-poll-h fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('test-list.index')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">View
                                                Tests
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header text-center text-primary">
                    <h5 class="mb-0">Dashboard/Samples/Reports</h5>
                </div>
                <div class="row card-body">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('back.view_sample')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">
                                                View Samples
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-vials fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <a href="{{route('report.index')}}">
                                            <div class="text-xl font-weight-bold text-primary text-uppercase mb-1">View
                                                Reports
                                            </div>
                                        </a>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-poll-h fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif

    </div>
    <!-- /.container-fluid -->
@endsection
