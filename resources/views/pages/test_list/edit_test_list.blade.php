@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-primary text-center">
                <h4 class="mb-0">Edit Test Lists</h4>
            </div>

            <div class="card-body col-md-12">
                <form action="{{route('test-list.update',$test_list->id)}}" method="post">
                    @method('PUT')
                    @csrf

                    <div class="row col-md-12 mb-4">
                        <div class="from-group col-md-6">
                            <label for="">Test Category</label>
                            <select name="test_category" id="" class="form-control">
                                <option value="" selected disabled="">Select test category</option>
                                @foreach($test_categories as $tc)
                                    <option
                                        value="{{$tc->id}}" {{$test_list->test_id == $tc->id?'selected':''}}>{{$tc->name}}</option>
                                @endforeach
                            </select>
                            @error('test_category')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="from-group col-md-6">
                            <label for="">Test Lists (
                                <small>Select only if Test List has parent category</small>
                                ) </label>
                            <select name="test_list_id" id="" class="form-control">
                                <option value="" selected disabled="">Select test category</option>
                                @foreach($test_lists as $tl)
                                    <option
                                        value="{{$tl->id}}" {{$test_list->parent_id == $tl->id?'selected':''}}>{{$tl->name}}</option>
                                @endforeach
                                <option value="">None</option>
                            </select>
                            @error('test_list_id')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="test_list_name"
                                   value="{{$test_list->name}}">
                            @error('test_list_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Unit</label>
                            <input type="text" class="form-control" name="unit" value="{{$test_list->unit}}">
                            @error('unit')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>

                        <div class="from-group col-md-4">
                            <label for="">Test Cost</label>
                            <input type="text" class="form-control" name="cost" value="{{$test_list->cost}}">
                            @error('cost')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for="">Description</label>
                            <input type="text" class="form-control" name="description"
                                   value="{{$test_list->description}}">
                            @error('description')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mt-4">
                        <div class="form-group col-md-4">
                            <label for="">Reference Range</label>
                            <input type="text" class="form-control" name="ref_range" value="{{unserialize($test_list->ref_range) }}">
                            @error('ref_range')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Normal</label>
                            <input type="text" class="form-control" name="normal" value="{{$test_list->normal}}">
                            @error('normal')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Abnormal</label>
                            <input type="text" class="form-control" name="abnormal" value="{{$test_list->abnormal}}">
                            @error('abnormal')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 mb-4 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
