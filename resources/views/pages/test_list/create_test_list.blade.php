@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0">Create Test Lists</h4>
            </div>

            <div class="card-body">
                <form action="{{route('test-list.store')}}" method="post">
                    @csrf
                    <div class="row col-md-12 mb-4">
                        <div class="from-group col-md-6">
                            <label for="">Test Category</label>
                            <select name="test_category" id="" class="form-control test-category">
                                <option value="" selected disabled="">Select test category</option>
                                @foreach($test_categories as $tc)
                                    <option
                                        value="{{$tc->id}}" {{old('test_category') == $tc->id?'selected':''}}>{{$tc->name}}</option>
                                @endforeach
                            </select>
                            @error('test_category')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="from-group col-md-6">
                            <label for="">Test Lists (
                                <small>Select only if Test List has parent category</small>)
                            </label>
                            <select name="test_list_id" class="form-control test-list">
                                <option value="" selected disabled="">Select test category</option>
                                @foreach($test_lists as $tl)
                                    <option
                                        value="{{$tl->id}}" {{old('test_list_id') == $tl->id?'selected':''}}>{{$tl->name}}</option>
                                @endforeach
                            </select>
                            @error('test_list_id')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="test_list_name"
                                   value="{{old('test_list_name')}}" placeholder="eg. WBC">
                            @error('test_list_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Unit</label>
                            <input type="text" class="form-control" name="unit" value="{{old('unit')}}"
                                   placeholder="eg. /cmm">
                            @error('unit')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Test Cost</label>
                            <input type="number" class="form-control" name="cost" value="{{old('cost')}}" placeholder="Rs.">
                            @error('cost')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for="">Description</label>
                            <input type="text" class="form-control" name="description"
                                   value="{{old('description')}}" placeholder="eg. White Blood Cell Count">
                            @error('description')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mt-4">
                        <div class="form-group col-md-4">
                            <label for="">Reference Range</label>
                            <input type="text" class="form-control" name="ref_range" value="{{old('ref_range')}}"
                                   placeholder="eg. 4000-11000">
                            @error('ref_range')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Normal</label>
                            <input type="text" class="form-control" name="normal" value="{{old('normal')}}"
                                   placeholder="eg. 5000">
                            @error('normal')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Abnormal</label>
                            <input type="text" class="form-control" name="abnormal" value="{{old('abnormal')}}"
                                   placeholder="eg. 10000">
                            @error('abnormal')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
@section('script')
    <script>
        $(document).ready(function () {
            $('.test-category').select2({
                placeholder: 'Select Test Category',
                theme:'classic',
            });
            $('.test-list').select2({
                placeholder: 'Select Test Category',
                theme:'classic',

            });
        })
    </script>
@stop
