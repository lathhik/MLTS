@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card mb-4">
            <div class="card-header ">
                <h4 class="m-0 font-weight-bold text-primary">Test Lists
                    <a href="{{route('test-list.create')}}" class="btn btn-sm btn-primary float-right">
                        <li class="fa fa-plus"></li>
                        Create Test List</a></h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered view-table" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Ref Range</th>
                            <th>Normal</th>
                            <th>Abnormal</th>
                            <th>Test Category</th>
                            <th>Cost</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($test_lists as $test_list)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$test_list->name}}</td>
                                <td>{{$test_list->unit}}</td>
                                <td>{{unserialize($test_list->ref_range)}}</td>
                                <td>{{$test_list->normal}}</td>
                                <td>{{$test_list->abnormal}}</td>
                                <td>
                                    {{$test_list->test->name}}
                                    {{$test_list->parent_id?'-':''}}
                                    {{\App\Models\TestList::where('id',$test_list->parent_id)->value('name')}}
                                </td>
                                <td>Rs. {{$test_list->cost}}</td>
                                <td>
                                    <a href="{{route('test-list.edit',$test_list->id)}}"
                                       class="btn btn-sm btn-secondary mb-1"><span
                                            class="fa fa-edit"></span></a>
                                    <form action="{{route('test-list.destroy', $test_list->id)}}" method="post"
                                          class="form-check-inline">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger"  onclick="return(confirm('Are You Sure'))"><span
                                                class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
