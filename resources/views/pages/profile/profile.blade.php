@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0 d-inline">Profile</h4>
                <a href="{{route('back.show_reset')}}" class="btn btn-primary float-right">Change Password</a>
            </div>
            <div class="card-body">
                @include('messages.message')
                <div class="row">
                    <div class="row col-md-12">
                        <div class="profile-image" style="margin-left: 310px">
                            <img
                                src="{{$admin->image?asset('assets/images/admin/'.$admin->image):asset('assets/images/alt.jpg')}}"
                                alt="" height="200px">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 row mt-4">
                    <div class="form-group col-md-4">
                        <label for="">Full Name</label>
                        <input type="text" class="form-control" value="{{$admin->full_name}}" readonly="readonly">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">Username</label>
                        <input type="text" class="form-control" value="{{$admin->username}}" readonly="readonly">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">Email</label>
                        <input type="text" class="form-control" value="{{$admin->email}}" readonly>
                    </div>
                </div>
                <div class="col-md-12 row">
                    <div class="form-group col-md-4">
                        <label for="">Address</label>
                        <input type="text" class="form-control" value="{{$admin->address}}" readonly>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">Phone</label>
                        <input type="text" class="form-control" value="{{$admin->phone}}" readonly>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="">Gender</label>
                        <input type="text" class="form-control" value="{{$admin->gender}}" readonly>
                    </div>
                </div>
                <div class="text-center">
                    <button class="btn btn-primary edit-profile"><a href="{{route('admin.edit',$admin->id)}}">Edit</a>
                    </button>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
