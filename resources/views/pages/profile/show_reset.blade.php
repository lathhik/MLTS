@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0 d-inline">Password Reset</h4>
            </div>
            <div class="card-body">
                @include('messages.message')
                <form action="{{route('back.reset_action')}}" method="post">
                    @csrf
                    <div class="col-md-12 row mt-4">
                        <div class="form-group col-md-4">
                            <label for="">Old Password</label>
                            <input type="password" class="form-control" value="" name="old_password">
                            @error('old_password')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12 row mt-4">
                        <div class="form-group col-md-4">
                            <label for="">New Password</label>
                            <input type="password" class="form-control" value="" name="password">
                            @error('password')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Confirm</label>
                            <input type="password" class="form-control" value="" name="password_confirmation">
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary edit-profile" type="submit">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop
