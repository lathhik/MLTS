@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0">Create Admin/Staffs</h4>
            </div>
            <div class="card-body">
                <form action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control" name="full_name" value="{{old('full_name')}}">
                            @error('full_name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">User Name</label>
                            <input type="text" class="form-control" name="username" value="{{old('username')}}">
                            @error('username')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}">
                            @error('email')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="address" value="{{old('address')}}">
                            @error('address')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{old('phone')}}">
                            @error('phone')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Gender</label><br>
                            <input type="radio" name="gender" value="M" id="male" {{old('gender') == 'M'?'checked':''}}>&nbsp;<label for="male">Male</label>&nbsp;
                            <input type="radio" name="gender" value="F" id="female" {{old('gender') == 'F'?'checked':''}}>&nbsp;<label
                                for="female">Female</label>&nbsp;
                            <input type="radio" name="gender" value="O" id="others" {{old('gender') == 'O'?'checked':''}}>&nbsp;<label
                                for="others">Others</label>
                            @error('gender')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Role</label>
                            <select name="role" id="" class="form-control">
                                <option value="" selected disabled>Select Role</option>
                                <option value="SA" {{old('role') == 'SA'?'selected':''}}>Admin</option>
                                <option value="RT" {{old('role') == 'RT'?'selected':''}}>Reception</option>
                                <option value="LT" {{old('role') == 'LT'?'selected':''}}>Lab Technician</option>
                            </select>
                            @error('role')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="from-group col-md-4">
                            <label for="">Image</label>
                            <input type="file" class="form-control-file" name="image">
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-4">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control">
                            @error('password')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                    </div>

                    <div class="text-center mt-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
