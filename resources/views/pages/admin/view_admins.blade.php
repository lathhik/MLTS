@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('messages.message')
        <div class="card  mb-4">
            <div class="card-header py-3 text-center">
                <h6 class="m-0 font-weight-bold text-primary">Admin / Staffs</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead class="text-center">
                        <tr>
                            <th>SN</th>
                            <th>Full Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Gender</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th width="11%">Action</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($admins as $admin)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$admin->full_name}}</td>
                                <td>{{$admin->username}}</td>
                                <td>{{$admin->email}}</td>
                                <td>{{$admin->address}}</td>
                                <td>{{$admin->phone}}</td>
                                <td>{{$admin->gender}}</td>
                                <td>
                                    @if($admin->role == 'RT')
                                        Reception
                                    @elseif($admin->role == 'LT')
                                        Lab Technician
                                    @else
                                        Admin
                                    @endif
                                </td>
                                <td>
                                    @if($admin->status == 1)
                                        <a href="{{route('admin.update_status', $admin->id)}}"
                                           class="btn btn-sm btn-success"><span class="fa fa-check"></span></a>
                                    @else
                                        <a href="{{route('admin.update_status', $admin->id)}}"
                                           class="btn btn-sm btn-danger"><span class="fa fa-times"></span></a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('admin.edit',$admin->id)}}" class="btn btn-sm btn-secondary mb-1"><span
                                            class="fa fa-edit"></span></a>
                                    <form action="{{route('admin.destroy', $admin->id)}}" method="post" class="form-check-inline">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return(confirm('Are you sure??'))"><span
                                                class="fa fa-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
