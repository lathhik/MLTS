@include('partials.head')
<!-- Page Wrapper -->
<div id="wrapper">
@include('partials.side_nav')
<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            @include('partials.top_nav')
            @yield('content')
        </div>
        <!--End of Main Content -->
        @include('partials.footer')
    </div>
    <!--End of Content Wrapper -->
</div>
<!--End of Page Wrapper -->
@include('partials.foot')
@yield('script')
