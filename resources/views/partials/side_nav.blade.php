<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion d-print-none" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('index')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-clinic-medical"></i>
        </div>
        <div class="sidebar-brand-text mx-3">
            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->role == 'SA')
                {{"Super Admin"}}
            @elseif(\Illuminate\Support\Facades\Auth::guard('admin')->user()->role == 'RT')
                {{"Reception"}}
            @elseif(\Illuminate\Support\Facades\Auth::guard('admin')->user()->role == 'LT')
                {{"Technician"}}
            @endif
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{request()->routeIs('index')?'active':''}}">
        <a class="nav-link" href="{{route('index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

@if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role == 'SA')
    <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            View/Create <span class="small">(admin/staffs)</span>
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{request()->routeIs('admin*')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#admins" aria-expanded="true"
               aria-controls="admins">
                <i class="fas fa-fw fa-user-secret"></i>
                <span>Admins/Staffs</span>
            </a>
            <div id="admins" class="collapse {{request()->routeIs('admin*')?'show':''}}" aria-labelledby="admins"
                 data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Custom Components:</h6>
                    <a class="collapse-item {{request()->routeIs('admin.index')?'active':''}}"
                       href="{{route('admin.index')}}">View</a>
                    <a class="collapse-item {{request()->routeIs('admin.create')?'active':''}}"
                       href="{{route('admin.create')}}">Create</a>
                </div>
            </div>
        </li>
@endif
@if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role == 'SA' || Auth::guard('admin')->user()->role == 'RT' )
    <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
            Patient <span class="small">(view/Create)</span>
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{request()->routeIs('patient*')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#patient" aria-expanded="true"
               aria-controls="patient">
                <i class="fas fa-fw fa-user-injured"></i>
                <span>Patient</span>
            </a>
            <div id="patient" class="collapse {{request()->routeIs('patient*')?'show':''}}" aria-labelledby="headingTwo"
                 data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Custom Components:</h6>
                    <a class="collapse-item {{request()->routeIs('patient.index')?'active':''}}"
                       href="{{route('patient.index')}}">View</a>
                    <a class="collapse-item {{request()->routeIs('patient.create')?'active':''}}"
                       href="{{route('patient.create')}}">Create</a>
                </div>
            </div>
        </li>
@endif
@if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role == 'RT' || Auth::guard('admin')->user()->role == 'SA')
    <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Invoice
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{request()->routeIs('back.view_to_generate_invoice')?'active':'' || request()->routeIs('back.generate_invoice')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#invoice"
               aria-expanded="true"
               aria-controls="invoice">
                <i class="fas fa-fw fa-money-bill"></i>
                <span>Invoice</span>
            </a>
            <div id="invoice" class="collapse {{request()->routeIs('back.view_to_generate_invoice')?'show':''}}"
                 aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{request()->routeIs('back.view_to_generate_invoice')?'active':''}}"
                       href="{{route('back.view_to_generate_invoice')}}">View</a>
                </div>
            </div>
        </li>
@endif
@if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role != 'RT')
    <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Tests
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{ request()->is('test/*')?'active':'' || request()->is('test')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#test_categories"
               aria-expanded="true"
               aria-controls="test_categories">
                <i class="fas fa-fw fa-list"></i>
                <span>Test Categories</span>
            </a>
            <div id="test_categories"
                 class="collapse {{ request()->is('test/*') ? 'show' : '' || request()->is('test')?'show':''}}"
                 aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{request()->routeIs('test.index')?'active':''}}"
                       href="{{route('test.index')}}">View</a>
                    <a class="collapse-item {{request()->routeIs('test.create')?'active':''}}"
                       href="{{route('test.create')}}">Create</a>
                </div>
            </div>
        </li>
        <li class="nav-item {{ request()->is('test-list/*')?'active':'' ||  request()->is('test-list')?'active':'' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#test_lists" aria-expanded="true"
               aria-controls="test_lists">
                <i class="fas fa-fw fa-list-alt"></i>
                <span>Test List</span>
            </a>
            <div id="test_lists"
                 class="collapse {{request()->is('test-list/*')?'show':'' || request()->is('test-list')?'show':''}}"
                 aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{request()->routeIs('test-list.index')?'active':''}}"
                       href="{{route('test-list.index')}}">View</a>
                    <a class="collapse-item {{request()->routeIs('test-list.create')?'active':''}}"
                       href="{{route('test-list.create')}}">Create</a>
                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Samples
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{request()->routeIs('*sample*')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#samples"
               aria-expanded="true"
               aria-controls="samples">
                <i class="fas fa-fw fa-vials"></i>
                <span>Samples</span>
            </a>
            <div id="samples" class="collapse {{request()->routeIs('*sample*')?'show':''}}"
                 aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{request()->routeIs('back.view_sample')?'active':''}}"
                       href="{{route('back.view_sample')}}">View</a>
                </div>
            </div>
        </li>
@endif
@if(Auth::guard('admin')->check() && Auth::guard('admin')->user()->role != 'RT')
    <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Reports
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{request()->routeIs('report*')?'active':''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#reports"
               aria-expanded="true"
               aria-controls="reports">
                <i class="fas fa-fw fa-poll-h"></i>
                <span>Reports</span>
            </a>
            <div id="reports" class="collapse {{request()->routeIs('report*')?'show':''}}"
                 aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{request()->routeIs('report.index')?'active':''}}"
                       href="{{route('report.index')}}">View</a>
                </div>
            </div>
        </li>
@endif
<!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->
