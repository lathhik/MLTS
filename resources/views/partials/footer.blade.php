
<!-- Footer -->
<footer class="sticky-footer bg-white d-print-none">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Medi Lab 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->
