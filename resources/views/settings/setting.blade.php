@extends('master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center text-primary">
                <h4 class="mb-0">Settings</h4>
            </div>
            <div class="card-body">
                <form action="{{route('settings.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="status" value="1">
                    <div class="row col-md-12">
                        <div class="form-group col-md-12">
                            <label for="">Organization Name</label>
                            <input type="text" class="form-control" name="name"
                                   value="{{$settings->name??old('name')}}">
                            @error('name')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Address(District)</label>
                            <input type="text" class="form-control" name="address_district"
                                   value="{{$settings->address_district??old('address_district')}}">
                            @error('address_district')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Address(City/Municipality/Area)</label>
                            <input type="text" class="form-control" name="address_area"
                                   value="{{$settings->address_area??old('address_area')}}">
                            @error('address_area')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Phone 1 </label>
                            <input type="text" class="form-control" name="phone_1"
                                   value="{{$settings->phone_1??old('phone_1')}}">
                            @error('phone_1')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Phone 2</label>
                            <input type="text" class="form-control" name="phone_2"
                                   value="{{$settings->phone_2??old('phone_2')}}">
                            @error('phone_2')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Image(Logo)</label>
                            <input type="file" class="form-control-file" name="image_logo"
                                   value="{{old('image_logo')}}">
                            @error('image_logo')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="text-center mt-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop
